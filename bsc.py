#Do Won Kim - Baseball Stats Counter

import re

import sys, os
 
if len(sys.argv) < 2:
	sys.exit("Usage: %s filename" % sys.argv[0]) #Usage Message

filename = sys.argv[1]
 
if not os.path.exists(filename):
	sys.exit("Error: File '%s' not found" % sys.argv[1])

player_list = []
batting_avg = []

f = open("cardinals-1940.txt", "r")
for line in f:
    stringName = re.match('^(([A-Z]{1}[a-z]+)+(\s[\w\-]+))', line.rstrip())
    if stringName is not None:
        strName = stringName.group()
        if strName not in player_list:
            player_list.append(strName)
f.close

for pName in player_list:
    f = open (filename, "r")
    bats = 0.0
    hits = 0.0
    for line in f:
        fullName = re.match('^(([A-Z]{1}[a-z]+)+(\s[\w\-]+))', line.rstrip())
        if fullName is not None:
            fName = fullName.group()
            if fName == pName:
                stats = re.findall(r'\d+', line.rstrip()) #find and store one or more (all) occurence of integers in the line read
                bat = float(stats[0])
                hit = float(stats[1])
                bats = bats + bat
                hits = hits + hit
    bat_avg = "%.3f" % ((hits)/(bats)) #round up to thousandth decimal place
    f.close
    batting_avg.append(bat_avg)

player_to_stat = dict(zip(player_list, batting_avg)) #create dictionary where playername:battingaverage
player_to_stat = player_to_stat.items() #create an array of the dictionary pairs
player_to_stat = sorted(player_to_stat, key = lambda x: float(x[1]), reverse = True) #sorts array by batting average, from highest to lowest average

for i in range (0,len(player_list)):
    print player_to_stat [i][0] + ": " + player_to_stat[i][1]